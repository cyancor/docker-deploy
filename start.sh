#!/bin/bash

function DeployFtp()
{
	protocol="$1"
	hostport="$2"
	user="$3"
	password="$4"
	path="${5%/}"
	
	echo "Deploying to $protocol$hostport/$path..."
	if [[ $path != "" ]]; then
		lftp -e "mkdir -p $path; bye" -u "$user,$password" "$protocol$hostport" 2>&1
	fi
	
	Options="-v"
	Commands="set ftp:list-options -a;"
	if [ "$Sync" == "1" ]; then Options="$Options --delete"; fi
	if [ "$Parallel" != "" ]; then Options="$Options --parallel=$Parallel"; fi
	if [ "$Verbose" != "" ]; then Options="$Options --verbose"; fi
	if [ "$ActiveMode" != "" ]; then Commands="$Commands set ftp:passive-mode off;"; fi
	echo "$Commands mirror $Options"
	lftp -e "$Commands mirror $Options -R \"/artifacts\" \"$path\"; bye" -u "$user,$password" "$protocol$hostport" 2>&1
}

while read target; do
	protocol="`echo $target | grep '://' | sed -e's,^\(.*://\).*,\1,g'`"
	url=`echo $target | sed -e s,$protocol,,g`
	userpass="`echo $url | grep @ | rev | cut -d@ -f2- | rev`"
	pass=`echo $userpass | grep : | cut -d: -f2`
	if [ -n "$pass" ]; then
		user=`echo $userpass | grep : | cut -d: -f1`
	else
		user=$userpass
	fi

	hostport=`echo $url | sed -e s,$userpass@,,g | cut -d/ -f1`
	port=`echo $hostport | grep : | cut -d: -f2`
	if [ -n "$port" ]; then
		host=`echo $hostport | grep : | cut -d: -f1`
	else
		host=$hostport
	fi

	path="`echo $url | grep / | cut -d/ -f2-`"


	if [[ "$protocol" == "sftp://" ]]; then DeployFtp "$protocol" "$hostport" "$user" "$pass" "$path"; fi
	if [[ "$protocol" == "ftp://" ]]; then DeployFtp "$protocol" "$hostport" "$user" "$pass" "$path"; fi
done </targets.txt


