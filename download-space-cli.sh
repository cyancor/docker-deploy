#!/bin/bash

sudo docker pull public.registry.jetbrains.space/p/space/containers/space-cli:latest
sudo docker run -d --name space-cli public.registry.jetbrains.space/p/space/containers/space-cli:latest
sudo docker cp space-cli:/usr/bin/space ./assets/space
sudo docker stop space-cli
sudo docker rm space-cli
