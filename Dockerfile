FROM ubuntu:latest

LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

COPY *.sh /
COPY assets/space /usr/bin/space

RUN apt-get update \
    && apt-get upgrade --yes \
    && apt-get install --yes \
      lftp \
      ssh \
      git \
      zip \
      unzip \
      p7zip-full \
      curl \
      wget \
    && mkdir /artifacts && chmod 0777 /artifacts \
    && mkdir -p ~/.ssh/ \
    && chmod +x /*.sh \
# Cleanup
    && apt-get clean all \
    && rm -rf /var/lib/apt/lists/*
